**programacion**
5AVP
Garcia Manzo Andrea Carolina

- Práctica #1 - 02/09/2022 - Práctica de ejemplo
Commit: 542a0d4dd9ea54758d0fc3e457d67af030e0726a
Archivo: https://gitlab.com/GarciaCar777/desarrolla_aplicaciones_web/-/blob/main/parcial1/practica_ejemplo.html

- Práctica #2 - 09/09/2022 - Práctica 2 Mensaje JavaScript
Commit: 14633e222b472b1afeac1547d788720393725e9f
Archivo: https://gitlab.com/GarciaCar777/desarrolla_aplicaciones_web/-/blob/main/parcial1/Practica2JavaScript.html

- Práctica #3 - 15/09/22 - Práctica web con base de datos - parte 1
Commit: 5a8e4dedd1ecec2c478ab450d00e867a26f2521d Archivo: https://gitlab.com/GarciaCar777/desarrolla_aplicaciones_web/-/blob/main/parcial1/PaginaWebDatos/index.html

- Práctica #4 - 19/09/22 - Práctica web con bases de datos - Vista de consulta de datos
Commit: 51cbefea50d2a0cbcd0b8111ebec5a274c1fab27
Archivo: https://gitlab.com/GarciaCar777/desarrolla_aplicaciones_web/-/blob/main/parcial1/PaginaWebDatos/ConsultarDatos.php

- Práctica #5 - 19/09/22 - Práctica web con bases de datos - Vista de registro de datos
Commit: 33d2473fddcc9aab5b8746d256af00baadda9604
Archivo: https://gitlab.com/GarciaCar777/desarrolla_aplicaciones_web/-/blob/main/parcial1/PaginaWebDatos/registrarDatos.html

- Práctica #6 - 19/09/22 - Mostrar registros - Commit: 9422d3e792d029dcdfae74f0fb3e5818098b2838
Archivo: https://gitlab.com/GarciaCar777/desarrolla_aplicaciones_web/-/blob/main/parcial1/PaginaWebDatos/Conexion.php

